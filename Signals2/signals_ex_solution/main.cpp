#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class Printer
{
public:
    void print(const string& msg, double arg)
    {
        cout << msg << arg << endl;
    }
};

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
};

void save_to_db(double temp)
{
    cout << "Saving to db: " << temp << endl;
}

class TemperatureMonitor
{
public:
    // zdefiniować typ sygnału TemperatureChangedSignal
    typedef boost::signals2::signal<void (double)> TemperatureChangedSignal;

    // zdefiniować typ slotów dla sygnału
    typedef TemperatureChangedSignal::slot_type TemperatureChangedSlot;

    TemperatureMonitor() : temp_(0.0) {}

    // implementacja podłączenia slotu do sygnału
    boost::signals2::connection attach(TemperatureChangedSlot slot)
    {
        return temp_changed_.connect(slot);
    }

    void set_temp(double temp)
    {
        if (temp_ != temp)
        {
            temp_ = temp;
            temp_changed_(temp_);  // emisja sygnalu
        }
    }

private:
    TemperatureChangedSignal temp_changed_; // sygnał zmiany temperatury
    double temp_;
};
int main()
{
    using namespace boost::signals2;

    Printer printer;
    TemperatureMonitor monitor;
    Logger logger;

    // podłączenie Printera
    monitor.attach(boost::bind(&Printer::print, &printer, "Printing new temp: ", _1));

    // podłączenie Loggera
    connection conn2log = monitor.attach(boost::bind(&Logger::log, &logger, boost::bind(&boost::lexical_cast<string, double>, _1)));

    monitor.set_temp(24.0);
    monitor.set_temp(45.0);

    {
        // podłączenie save_to_db - ograniczone do zakresu
        scoped_connection scoped_conn(monitor.attach(&save_to_db));

        monitor.set_temp(124.0);
        monitor.set_temp(245.0);
    }

    monitor.set_temp(45.0);

    {
        // podłączenie z shared_ptr<Printer>
        boost::shared_ptr<Printer> ptr_printer(new Printer);

        monitor.attach(TemperatureMonitor::TemperatureChangedSlot(
                           &Printer::print, ptr_printer.get(), "Shared printer: ", _1).track(ptr_printer));

        monitor.set_temp(400.0);
    }

     monitor.set_temp(45.0);
}
