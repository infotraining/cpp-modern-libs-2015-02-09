#include <iostream>
#include <boost/bind.hpp>

class Tracer
{
public:
	Tracer()
	{
		std::cout << "Tracer::Tracer()" << std::endl;
	}

	Tracer(const Tracer& t)
	{
		std::cout << "Tracer::Tracer(const Tracer& t)" << std::endl;
	}

	Tracer& operator=(const Tracer& t)
	{
		std::cout << "Tracer::operator=(const Tracer& t)" << std::endl;
		return *this;
	}

	~Tracer()
	{
		std::cout << "Tracer::~Tracer()" << std::endl;
	}

	void print(const std::string& s) const
	{
		std::cout << s << "\n";
	}
};

int main()
{

	{
		Tracer t;

        // kopiowanie t przez wartosc
        boost::bind(&Tracer::print, t, _1)(std::string("Wywolanie na rzecz kopii t"));
	}

	std::cout << "---------------" << std::endl;

	{
		Tracer t;

		// przekazanie t jako referencji
        boost::bind(&Tracer::print, boost::ref(t), _1)("Wywolanie na rzecz referencji do t");
	}

	std::cout << "---------------" << std::endl;

	{
		Tracer t;

        // przekazanie t jako referencji do stalej
        boost::bind(&Tracer::print, boost::cref(t), _1)("Wywolanie na rzecz referencji do stałej t");
	}
}
