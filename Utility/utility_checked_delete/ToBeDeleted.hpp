#ifndef TOBEDELETED_HPP_
#define TOBEDELETED_HPP_

#include <iostream>

class ToBeDeleted
{
public:
	ToBeDeleted()
	{
		std::cout << "Konstruktor ToBeDeleted" << std::endl;
	}

	~ToBeDeleted()
	{
		std::cout << "Destruktor ~ToBeDeleted" << std::endl;
	}
};

#endif /* TOBEDELETED_HPP_ */
