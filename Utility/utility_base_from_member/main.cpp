#include <iostream>
#include <string>
#include <boost/utility/base_from_member.hpp>

class Logger
{
public:
	virtual void log(const std::string& message) = 0;
	virtual ~Logger() {}
};

class ConsoleLogger : public Logger
{
public:
	ConsoleLogger()
	{
		std::cout << "ConsoleLogger ctor\n";
	}

	void log(const std::string& message)
	{
		std::cout << "Log: " << message << std::endl;
	}
};

class Service
{
	Logger* logger_;
public:
	Service(Logger* logger) : logger_(logger)
	{
		logger_->log("Service ctor");
	}

	void run()
	{
		logger_->log("Start of work...");
		do_work();
		logger_->log("End of work...");
	}
protected:
	virtual void do_work() = 0;
};

namespace Before
{
	class MyService : public Service
	{
		ConsoleLogger logger_;
	public:
		MyService() : Service(&logger_)
		{

		}
	protected:
		void do_work()
		{
			std::cout << "Service is running..." << std::endl;
		}
	};
}

namespace After
{
	class MyService : private boost::base_from_member<ConsoleLogger>, public Service
	{
	public:
		MyService() : Service(&member)
		{}
	protected:
		void do_work()
		{
			std::cout << "Service is running..." << std::endl;
		}
	};
}



int main()
{
    Before::MyService srv1;
    srv1.run();

	std::cout << "\n-----------------------------------\n";

	After::MyService srv2;
	srv2.run();
}
