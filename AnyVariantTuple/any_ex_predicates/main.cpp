#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>

using namespace std;

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
     * 4 - Extra. Napisz klasę MapAny
	 */

	// 1
	vector<boost::any> non_empty;
	// TODO
	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
	int count_int;
	// TODO
    cout << "non_empty przechowuje " << count_int << " elementow typu int" << endl;

	int count_string;
	// TODO
    cout << "non_empty przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;
	// TODO

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;

    // 4
    /*
    MapAny m;

    m.insert("id", 1);
    m.insert("name", string("Adam"));
    m.insert("age", 33);

    try
    {
        string id = m.get<string>("id");
        cout << "id = " << id << endl;
        string name = m.get<string>("name");
        cout << "name = " << name << endl;
        string address = m.get<string>("address");
    }
    catch(InvalidValueType& e)
    {
        cout << e.what() << endl;
    }
    catch(KeyNotFound& e)
    {
        cout << e.what() << endl;
    }
    */
}
