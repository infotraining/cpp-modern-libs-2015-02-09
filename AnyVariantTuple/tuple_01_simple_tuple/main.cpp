#include <iostream>
#include <string>
#include <tuple>
#include <functional>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <numeric>

using namespace std;

template <typename Tuple, unsigned int Index>
struct PrintHelper
{
    static void print_tuple(const Tuple& t)
    {
        PrintHelper<Tuple, Index-1>::print_tuple(t);
        cout << get<Index-1>(t) << " ";
    }
};

template <typename Tuple>
struct PrintHelper<Tuple, 0>
{
    static void print_tuple(const Tuple& t)
    {
    }
};

template <typename... T>
void print_tuple(const tuple<T...>& t, const string& prefix)
{
    cout << prefix << ": [ ";
    PrintHelper<decltype(t), sizeof...(T)>::print_tuple(t);
    cout << "]" << endl;
}

tuple<int, int, double> calculate_stats(const vector<int>& data)
{
    auto min = *min_element(data.begin(), data.end());
    auto max = *max_element(data.begin(), data.end());
    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(min, max, avg);
}

int main()
{
	// krotka
	tuple<int, double, string> triple(43, 3.1415, "Krotka...");

	// odwolania do krotki
	cout << "triple = ("
		 << get<0>(triple) << ", "
		 << get<1>(triple) << ", "
		 << get<2>(triple) << ")" << endl;


    // domyslna inicjalizacja krotki
	tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
	triple = make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

    get<2>(triple) = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

	tuple<int&, string&> ref_tpl(x, str);
	get<0>(ref_tpl)++;
    boost::to_upper(get<1>(ref_tpl));

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

	tuple<const int&, string&> cref_tpl = make_tuple(cref(x), ref(str));

	//get<0>(cref_tpl)++; // Blad! Referencja do stalej!
	boost::to_lower(get<1>(cref_tpl));

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    vector<int> data = { 5, 1, 35, 321, 23, 5, 9, 88, 44, 324 };

    auto stats = calculate_stats(data);

    print_tuple(stats, "Results");
}
