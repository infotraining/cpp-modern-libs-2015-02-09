#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
    unsigned int devno_;
public:
	Device(unsigned int devno) : devno_{devno}
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Constructor of Device #" << devno << endl;
	}

    unsigned int device_no() const
    {
        return devno_;
    }

    virtual void run()
    {
        cout << "Device(id: " << devno_ << ")::run()" << endl;
    }

	virtual ~Device()
	{
		cout << "Destructor of Device #" << devno_ << endl;
	}
};

class SuperDevice : public Device
{
public:
    SuperDevice(unsigned int devno) : Device{devno}
    {
        cout << "Constructor of SuperDevice # " << devno << endl;
    }

    ~SuperDevice()
    {
        cout << "Destructor of SuperDevice #" << device_no() << endl;
    }

    void run() override
    {
        cout << "SuperDevice(id: " << device_no() << ")::run()" << endl;
    }
};

Device* create_device(unsigned int devno)
{
    if (devno % 2 == 0)
        return new Device(devno);
    else
        return new SuperDevice(devno);
}

class Broker 
{
public:
	Broker(int devno1, int devno2) : dev1_(NULL), dev2_(NULL)
	{
		// TODO - poprawa odpornosci na wyjatki

        dev1_ = create_device(devno1);
        dev2_ = create_device(devno2);
	}

    void start()
    {
        dev1_->run();
        dev2_->run();
    }

	~Broker()
	{
		delete dev1_;
		delete dev2_;
	}
private:
	Device* dev1_;
	Device* dev2_;
};

int main()
{
	try
	{
		Broker b(1, 2);
        b.start();
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
