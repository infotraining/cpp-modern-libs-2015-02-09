#include <iostream>
#include <string>
#include <iterator>
#include <functional>
#include <vector>

using namespace std;
//using namespace boost::lambda;

string formatter(const string& prefix, double d)
{
	return prefix + " " + to_string(d);
}

struct LenghtComparer 
{
public:
	using result_type = bool;
	using fist_argument_type = string;
	using second_argument_type = string;

	bool operator()(const std::string& s1, const std::string& s2) const
	{
		return s1.size() < s2.size();
	}
};

int main()
{
	try
	{
		// niezainicjowany obiekt function
		function<string (string, double)> fn0;
		auto result = fn0("Pi: ", 3.14);
	}
	catch(const bad_function_call& e)
	{
		cout << "Exception caught: " << e.what() << endl;
	}

	cout << "--------------------------------\n";

	// 1 - adres funkcji
	function<string (string, double)> fn1;

	// przypisanie adresu funkcji
	fn1 = &formatter;

	cout << "fn1('Pi: ', 3.14) = " << fn1("Pi:", 3.14) << endl;

	cout << "--------------------------------\n";

	// 2 - adres metody
	vector<int> vec = { 1, 2, 3 };

	function<void (vector<int>*, size_t)> fn2;

	fn2 = &vector<int>::reserve;
	
	if (fn2)
		fn2(&vec, 100);
	else
		cout << "fn2 is empty..." << endl;
	
	cout << "vec size: " << vec.size() << "; vec capacity: " << vec.capacity() << endl;


	cout << "--------------------------------\n";

	// 3 - obiekt funkcyjny

	function<bool (string, string)> fn3;

	fn3 = LenghtComparer();

	cout << "fn3(string(\"Dyzma\"), string(\"Dymski\")) = " << fn3("Dyzma", "Dymski") << endl;

	cout << "--------------------------------\n";

    // 4 - obiekt wiazania bind
	function<bool (string)> fn4 = bind(LenghtComparer(), placeholders::_1, "Dymski");

	cout << "fn4(\"Dyzma\") = " << fn4("Dyzma") << endl;

	cout << "--------------------------------\n";

	//5 - wyra�enie lambda
	function<void (int, int)> fn5;

	int x = 5;
	int y = 10;

	fn5 = [](int x, int y) { cout << "x: " << x << "; y: " << y << endl; };

	fn5(x, y);
}
